
 CBL_OC_SOCKET - replacement for ACUCOBOL C$SOCKET
 
 cob_socket (exported as CBL_OC_SOCKET) is in preperation of being
 integrated into the GnuCOBOL runtime library libcob.
 
 It can be used for any purpose (even outside of libcob) and is
 licensed under GNU Lesser General Public License version 3+.
 

 Build Hints:
 * for gcc: link against lstdc++
 * for VC: link against AdditionalDependencies="ws2_32.lib"

